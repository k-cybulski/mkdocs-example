# Example MkDocs site
This is an examke MkDocs site deployed on GitLab pages. It is rendered under [this link](https://k-cybulski.gitlab.io/mkdocs-example/).

The entire configuration necessary to run this is contained in the `.gitlab-ci.yml` file. To make a page like this, it should be sufficient to just fork this repository on GitLab and accordingly change the `site_url` in `mkdocs.yml`.
