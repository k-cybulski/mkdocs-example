# How to set up a website on GitLab pages
To set up a site on GitLab pages, you can either clone a repository like [this
example one](https://gitlab.com/k-cybulski/mkdocs-example) or make a `.gitlab-ci.yml`
configuration file, where you define commands necessary to make the site.

For example, for this little mkdocs page the `.gitlab-ci.yml` config looks like this:
```yaml
image: python:3.9
  # The image above comes from Docker Hub, which is a site with lots of
  # pre-configured little Linux "virtual machines"
  # python:3.9 above was chosen after googling "Python docker images" and
  # picking an arbitrary image from https://hub.docker.com/_/python

before_script:
  - pip install mkdocs https://github.com/mitya57/python-markdown-math/archive/master.zip

pages:
  script:
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
  - master
```

If you've done some terminal coding, you will see that `before_script` and
`pages`->`script` sections are exactly what you'd write in a Linux machine to deploy
the mkdocs site locally.

## Can you do LaTeX in this static page generator?
I'm glad you asked! Indeed you can. Check out this definition of the Dirac delta
$$\int_{-\infty}^{\infty}{f(x)\delta(x)dx} = f(0)$$

This is possible, because in the `mkdocs.yml` I import MathJax in `extra_javascript`
and enable the `mdx_math` extension.
